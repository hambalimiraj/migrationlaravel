<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('isi')->nullable();
            $table->unsignedBigInteger('profile_id')->constrained()->onDelete('cascade');
            $table->unsignedBigInteger('jawaban_id')->constrained()->onDelete('cascade');
            $table->foreign('jawaban_id')->references('id')->on('jawaban');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_jawaban', function (Blueprint $table) {
            $table->dropForeign(['profile_id']);    
            $table->dropColumn(['profile_id']);
            $table->dropForeign(['jawaban_id']);    
            $table->dropColumn(['jawaban_id']);
        });
    }
}
