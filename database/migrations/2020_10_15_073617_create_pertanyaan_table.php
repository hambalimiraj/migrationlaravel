<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('jawaban_id')->constrained()->onDelete('cascade');
            $table->unsignedBigInteger('profile_id')->constrained()->onDelete('cascade');
            $table->foreign('profile_id')->references('id')->on('profile');     
            $table->string('judul', 45);
            $table->longText('isi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['profile_id']);    
            $table->dropColumn(['profile_id']);
            $table->dropForeign(['jawaban_tepat_id']);    
            $table->dropColumn(['jawaban_tepat_id']);
        });
    }
}
