<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longText('isi')->nullable();
            $table->unsignedBigInteger('profile_id')->constrained()->onDelete('cascade');
            $table->unsignedBigInteger('pertanyaan_id')->constrained()->onDelete('cascade');
            $table->foreign('pertanyaan_id')->references('id')->on('pertanyaan');
            $table->foreign('profile_id')->references('id')->on('profile');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['pertanyaan_id']);    
            $table->dropColumn(['pertanyaan_id']);
            $table->dropForeign(['profile_id']);    
            $table->dropColumn(['profile_id']);
        });
    }
}